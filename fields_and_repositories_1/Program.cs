﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fields_and_repositories_1
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    //this class only has fields
    class water_botle_1
    {
        //this is the field
        //note that is is private and hence already hidden and inaccessible from outside folks
        private string quantity;

        //method that will set the quantity
        public void set_quantity(string temp_quantity)
        {
            quantity = temp_quantity;
        }

        //method that will return quantity
        public string get_quantity()
        {
            return quantity;
        }
    }

    class water_bottle_2
    {
        //this is the field
        //when working with properties, it is standard convention to use _ (under score) 
        //before field names. This indicates that a separate property that is taking 
        //care of it
        private string _quantity;

        //now instead of write two methods to do the reading and writing
        //I will use the accessors to create a property that wraps around the above field
        //of quantity
        private string Quantity
        {
            //this gets or gives the value stored in _quantity
            get
            {
                return _quantity;
            }
            set //this sets the value to be stored in _quantity
            {
                //we need to check if the string is not null
                //we also need to check if the string is white space
                //if it is, we through a error
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException();
                }
                //if no exceptiosn were thrown, that means the string is in fine shape
                //lets collect that value and assign it to our field.
                _quantity = value;

                //now, do note that the 'value' itself is never declared or defined. 
                //It is simply part of the package


            }
        }

        //as you have noticed above, its a lot of hardwork, just to add that extra layer of 
        //encapsulation. Hence c sharp allows a short hand notation

        private string Quantity2 { get; set; }

        //so this one line equals the everything we have done for the other property Quantity. 
        //so, who will write the rest of the code for Quantity2, if it is a property? 
        //the compiler of course, saving you valuable time.
    }


}
